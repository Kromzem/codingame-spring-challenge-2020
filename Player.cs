/**
 * Grab the pellets as fast as you can!
 **/

using System;
using System.Collections.Generic;
using System.Linq;

internal class Player {
    private static void Main(string[] args) {
        string[] inputs;
        inputs = Console.ReadLine().Split(' ');
        var width = int.Parse(inputs[0]); // size of the grid
        var height = int.Parse(inputs[1]); // top left corner is (x=0, y=0)
        for (var i = 0; i < height; i++)
        {
            var row = Console.ReadLine(); // one line of the grid: space " " is floor, pound "#" is wall
        }

        var pacs = new List<Pac>();
        var pallets = new List<Pallet>();
        
        
        // game loop
        while (true)
        {
            inputs = Console.ReadLine().Split(' ');
            var myScore = int.Parse(inputs[0]);
            var opponentScore = int.Parse(inputs[1]);

            var visiblePacCount = int.Parse(Console.ReadLine()); // all your pacs and enemy pacs in sight
            for (var i = 0; i < visiblePacCount; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                var pacId = int.Parse(inputs[0]); // pac number (unique within a team)
                var mine = inputs[1] != "0"; // true if this pac is yours
                var x = int.Parse(inputs[2]); // position in the grid
                var y = int.Parse(inputs[3]); // position in the grid
                var typeId = inputs[4]; // unused in wood leagues
                var speedTurnsLeft = int.Parse(inputs[5]); // unused in wood leagues
                var abilityCooldown = int.Parse(inputs[6]); // unused in wood leagues

                if (mine)
                {
                    pacs.Add(new Pac {
                        X = x,
                        Y = y,
                        Id = pacId
                    });
                }
            }

            var visiblePelletCount = int.Parse(Console.ReadLine()); // all pellets in sight
            for (var i = 0; i < visiblePelletCount; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                var x = int.Parse(inputs[0]);
                var y = int.Parse(inputs[1]);
                var value = int.Parse(inputs[2]); // amount of points this pellet is worth
                
                pallets.Add(new Pallet {
                    X = x,
                    Y = y,
                    Value = value
                });
            }

            foreach (var pac in pacs)
            {
                var bestPallet = (Pallet) null;
                foreach (var pallet in pallets)
                {
                    if (bestPallet == null)
                    {
                        bestPallet = pallet;
                        continue;
                    }

                    if (pallet.Value > bestPallet.Value)
                    {
                        bestPallet = pallet;
                    }
                }
                
                Console.WriteLine($"MOVE {pac.Id} {bestPallet.X} {bestPallet.Y}"); // MOVE <pacId> <x> <y>
            }
        }
    }

    private class Pac {
        public int X { get; set; }
        public int Y { get; set; }
        public int Id { get; set; }
    }

    private class Pallet {
        public int X { get; set; }
        public int Y { get; set; }
        public int Value { get; set; }
    }
}